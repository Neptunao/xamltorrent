﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using xamlTorrent.ViewModel;

namespace xamlTorrent.Model {
    public interface ITorrentObject {
        void OnPartDownloaded();
    }
    public abstract class TorrentObject : IconObject {
        public static readonly DependencyProperty SizeProperty = DependencyProperty.Register("Size", typeof(double), typeof(TorrentObject), new PropertyMetadata(0d));
        public static readonly DependencyProperty DownloadedSizeProperty = DependencyProperty.Register("DownloadedSize", typeof(double), typeof(TorrentObject),
            new PropertyMetadata(0d, (d, e) => ((TorrentObject)d).OnDownloadedSizeChanged(), (d, e) => ((TorrentObject)d).CoerceDownloadedSize((double)e)));
        static readonly DependencyPropertyKey PercentageCompletedPropertyKey = DependencyProperty.RegisterReadOnly("PercentageCompleted", typeof(int), typeof(TorrentObject), new PropertyMetadata(0));
        public static readonly DependencyProperty PathProperty = DependencyProperty.Register("Path", typeof(string), typeof(TorrentObject), new PropertyMetadata());
        public static readonly DependencyProperty PercentageCompletedProperty = PercentageCompletedPropertyKey.DependencyProperty;
        public static readonly DependencyProperty OpenFolderCommandProperty = DependencyProperty.Register("OpenFolderCommand", typeof(ICommand), typeof(TorrentObject));
        public static readonly DependencyProperty StopProperty = DependencyProperty.Register("Stop", typeof(DelegateCommand), typeof(TorrentObject));
        protected TorrentObject() {
            OpenFolderCommand = new DelegateCommand<string>(obj => OpenDirectory(), obj => CanOpenDirectory());
            StopCommand = new DelegateCommand(obj => Stop());
        }
        private object CoerceDownloadedSize(double newValue) {
            return newValue > Size ? Size : newValue;
        }
        private bool CanOpenDirectory() {
            return Directory.Exists(FolderPath);
        }
        private void OpenDirectory() {
            var runExplorer = new ProcessStartInfo() { FileName = "explorer.exe", Arguments = FolderPath };
            Process.Start(runExplorer);
        }
        protected internal abstract void Stop();
        private void OnDownloadedSizeChanged() {
            PercentageCompleted = (int)(Math.Truncate(DownloadedSize / Size) * 100);
        }
        public int PercentageCompleted {
            get { return (int)GetValue(PercentageCompletedProperty); }
            private set { SetValue(PercentageCompletedPropertyKey, value); }
        }
        public double DownloadedSize {
            get { return (double)GetValue(DownloadedSizeProperty); }
            set { SetValue(DownloadedSizeProperty, value); }
        }
        public double Size {
            get { return (double)GetValue(SizeProperty); }
            set { SetValue(SizeProperty, value); }
        }
        public string Path {
            get { return (string)GetValue(PathProperty); }
            set { SetValue(PathProperty, value); }
        }
        public ICommand OpenFolderCommand {
            get { return (ICommand)GetValue(OpenFolderCommandProperty); }
            set { SetValue(OpenFolderCommandProperty, value); }
        }
        public DelegateCommand StopCommand {
            get { return (DelegateCommand)GetValue(StopProperty); }
            set { SetValue(StopProperty, value); }
        }
        private string FolderPath { get { return System.IO.Path.GetDirectoryName(Path); } }
    }
    public class Torrent : TorrentObject, ITorrentObject {
        public static readonly DependencyProperty NumberProperty =
            DependencyProperty.Register("Number", typeof(int), typeof(Torrent), new PropertyMetadata(-1));
        public static readonly DependencyProperty NameProperty =
            DependencyProperty.Register("Name", typeof(string), typeof(Torrent), new PropertyMetadata());
        public static readonly DependencyProperty StateProperty =
            DependencyProperty.Register("State", typeof(State), typeof(Torrent), new PropertyMetadata());
        public static readonly DependencyProperty CategoryProperty =
            DependencyProperty.Register("Category", typeof(Category), typeof(Torrent), new PropertyMetadata());
        public static readonly DependencyProperty FilesProperty =
            DependencyProperty.Register("Files", typeof(ObservableCollection<TorrentFile>), typeof(Torrent),
            new PropertyMetadata((d, e) => ((Torrent)d).OnFilesChanged((INotifyCollectionChanged)e.OldValue)));
        public static readonly DependencyProperty PauseCommandProperty = DependencyProperty.Register("PauseCommand", typeof(DelegateCommand), typeof(Torrent));
        public static readonly DependencyProperty PeerCountProperty = DependencyProperty.Register("PeerCount", typeof(int), typeof(Torrent));
        public static readonly DependencyProperty SeedCountProperty = DependencyProperty.Register("SeedCount", typeof(int), typeof(Torrent));
        public static readonly DependencyProperty SpeedProperty = DependencyProperty.Register("Speed", typeof(double), typeof(Torrent));
        public static readonly DependencyProperty StartedAtProperty =  DependencyProperty.Register("StartedAt", typeof(DateTime), typeof(Torrent), new PropertyMetadata(DateTime.Now));
        public Torrent(StateCollection states) {
            Files = new ObservableCollection<TorrentFile>();
            States = states;
            PauseCommand = new DelegateCommand(obj => Pause(), obj => State.StateId == StateId.InProgress);
        }
        private void OnFilesChanged(INotifyCollectionChanged oldValue) {
            if(oldValue != null)
                oldValue.CollectionChanged -= UpdateSizeFromFiles;
            Files.CollectionChanged += UpdateSizeFromFiles;
            UpdateSizeFromFilesCore();
        }
        private void UpdateSizeFromFiles(object sender, NotifyCollectionChangedEventArgs e) {
            UpdateSizeFromFilesCore();
        }
        private void UpdateSizeFromFilesCore() {
            Size = Files.Sum(f => f.Size);
        }
        public void StartDownload() {
            State = States.GoToRunState(State);
            PauseCommand.RaiseCanExecuteChanged();
            foreach(var file in Files) {
                file.StartDownload();
            }
            Random rnd = new Random();
            PeerCount = rnd.Next(50);
            SeedCount = rnd.Next(50);
            Speed = rnd.NextDouble() * 100;
        }
        private void StopFiles() {
            Speed = 0;
            foreach(var file in Files) {
                file.Stop();
            }
        }
        protected internal override void Stop() {
            StopFiles();
            State = States.GoToStopState(State);
            PauseCommand.RaiseCanExecuteChanged();
            SeedCount = 0;
            PeerCount = 0;
        }
        private void Pause() {
            StopFiles();
            State = States[StateId.Paused];
        }
        public ObservableCollection<TorrentFile> Files {
            get { return (ObservableCollection<TorrentFile>)GetValue(FilesProperty); }
            set { SetValue(FilesProperty, value); }
        }
        public Category Category {
            get { return (Category)GetValue(CategoryProperty); }
            set { SetValue(CategoryProperty, value); }
        }
        public State State {
            get { return (State)GetValue(StateProperty); }
            set { SetValue(StateProperty, value); }
        }
        public string Name {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }
        public int Number {
            get { return (int)GetValue(NumberProperty); }
            set { SetValue(NumberProperty, value); }
        }
        public DelegateCommand PauseCommand {
            get { return (DelegateCommand)GetValue(PauseCommandProperty); }
            set { SetValue(PauseCommandProperty, value); }
        }
        public int SeedCount {
            get { return (int)GetValue(SeedCountProperty); }
            set { SetValue(SeedCountProperty, value); }
        }
        public int PeerCount {
            get { return (int)GetValue(PeerCountProperty); }
            set { SetValue(PeerCountProperty, value); }
        }
        public double Speed {
            get { return (double)GetValue(SpeedProperty); }
            set { SetValue(SpeedProperty, value); }
        }
        public DateTime StartedAt {
            get { return (DateTime)GetValue(StartedAtProperty); }
            set { SetValue(StartedAtProperty, value); }
        }
        private StateCollection States { get; set; }
        void ITorrentObject.OnPartDownloaded() {
            DownloadedSize = Files.Sum(f => f.DownloadedSize);
            if(DownloadedSize >= Size) State = States[StateId.Distributed];
        }
    }
}
