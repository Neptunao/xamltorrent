﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace xamlTorrent.Model {
    public enum Priority {
        NotLoad,
        Low,
        Medium,
        High
    }

    public class TorrentFile : TorrentObject {
        public static IEnumerable<Priority> Priorities { get { return Enum.GetValues(typeof(Priority)).Cast<Priority>(); } }
        public static readonly DependencyProperty PriorityProperty = DependencyProperty.Register("Priority", typeof(Priority), typeof(TorrentFile), new PropertyMetadata(Priority.Medium));
        readonly DispatcherTimer timer = new DispatcherTimer();
        public TorrentFile(ITorrentObject owner) : base() {
            Owner = owner;
            this.timer.Tick += (s, e) => DownloadedPart();
        }
        public Priority Priority {
            get { return (Priority)GetValue(PriorityProperty); }
            set { SetValue(PriorityProperty, value); }
        }
        public ITorrentObject Owner { get; private set; }
        public void StartDownload() {
            this.timer.Interval = TimeSpan.FromMilliseconds(100);
            this.timer.Start();
        }
        private void DownloadedPart() {
            if(DownloadedSize >= Size) {
                this.timer.Stop();
                return;
            }
            DownloadedSize += 0.5;
            Owner.OnPartDownloaded();
        }
        protected internal override void Stop() {
            this.timer.Stop();
        }
    }
}
