﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace xamlTorrent.Model {
    public abstract class IconObject : DependencyObject {
        public static readonly DependencyProperty IconUriProperty = DependencyProperty.Register("IconUri", typeof(Uri), typeof(IconObject));
        public Uri IconUri {
            get { return (Uri)GetValue(IconUriProperty); }
            set { SetValue(IconUriProperty, value); }
        }
    }
    public abstract class Includable<T> : IconObject where T : class {
        protected Includable() { }
        public bool Include(T obj) {
            return IsIncludeAll ? true : IncludeCore(obj);
        }
        protected abstract bool IncludeCore(T obj);
        public bool IsIncludeAll { get; set; }
    }
    public class Category : Includable<Torrent> {
        public static readonly DependencyProperty NameProperty;
        static readonly DependencyPropertyKey CategoriesPropertyKey;
        public static readonly DependencyProperty CategoriesProperty;
        static Category() {
            Type ownerType = typeof(Category);
            NameProperty = DependencyProperty.Register("Name", typeof(string), ownerType);
            CategoriesPropertyKey = DependencyProperty.RegisterReadOnly("Categories", typeof(ObservableCollection<Category>), ownerType,
                new PropertyMetadata(new ObservableCollection<Category>()));
            CategoriesProperty = CategoriesPropertyKey.DependencyProperty;
        }
        public string Name {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }
        public ObservableCollection<Category> Categories {
            get { return (ObservableCollection<Category>)GetValue(CategoriesProperty); }
        }
        protected override bool IncludeCore(Torrent obj) {
            return obj.Category == this;
        }
    }
    public class State : Includable<Torrent> {
        public static readonly DependencyProperty NameProperty;
        static State() {
            Type ownerType = typeof(State);
            NameProperty = DependencyProperty.Register("Name", typeof(string), ownerType);
        }
        public string Name {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }
        public StateId StateId { get; set; }
        protected override bool IncludeCore(Torrent obj) {
            return obj.State == this;
        }
    }
    public class StateCollection : ObservableCollection<State> {
        public StateCollection() { }
        public StateCollection(List<State> list) : base(list) { }
        public StateCollection(IEnumerable<State> collection) : base(collection) { }
        public State this[StateId stateId] {
            get { return this.FirstOrDefault(s => s.StateId == stateId); }
        }
        public State GoToStopState(State currentState) {
            switch(currentState.StateId) {
                case StateId.Ready:
                case StateId.Stopped: return currentState;
                case StateId.Paused:
                case StateId.InProgress: return this[StateId.Stopped];
                case StateId.Distributed: return this[StateId.Ready];
                default: throw new NotImplementedException();
            }
        }
        public State GoToRunState(State currentState) {
            switch(currentState.StateId) {
                case StateId.Stopped:
                case StateId.Paused:
                case StateId.InProgress: return this[StateId.InProgress];
                case StateId.Ready:
                case StateId.Distributed: return this[StateId.Distributed];
                default: throw new NotImplementedException();
            }
        }
    }
    public enum StateId {
        All,
        Ready,
        InProgress,
        Stopped,
        Distributed,
        Paused
    }
}
