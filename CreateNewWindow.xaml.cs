﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace xamlTorrent {
    /// <summary>
    /// Interaction logic for CreateNewWindow.xaml
    /// </summary>
    public partial class CreateNewWindow : Window {
        public CreateNewWindow() {
            InitializeComponent();
        }
        private void btnOK_Click(object sender, RoutedEventArgs e) {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Torrent file (.torrent)|*.torrent";
            if(!(dlg.ShowDialog() == true)) return;
            DialogResult = true;
            Close();
        }
    }
}
