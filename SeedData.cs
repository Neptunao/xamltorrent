﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using xamlTorrent.Model;
using xamlTorrent.ViewModel;

namespace xamlTorrent {
    public class SeedDataBuilder {
        static List<string> SamplePaths {
            get {
                return new List<string>() { 
                    @"C:\",
                    @"D:\Torrents\",
                    @"C:\Downloads\"
                };
            }
        }
        static List<string> SampleNames { get { return new List<string>() { "Diablo 3", "Dota 2", "League of Legends", "Cloud Atlas", "The Fountain", "Kitaro" }; } }
        static List<string> SampleExtensions { get { return new List<string>() { "iso", "vob", "avi", "exe" }; } }
        private Category _Films;
        private Category _Games;
        private State _InProgress;
        private Category _MainCategory;
        private State _Paused;
        private State _Ready;
        private readonly Random _Random = new Random();
        private State _Stopped;
        private State _StateAll;
        private State _Distributed;
        ObservableCollection<Torrent> Torrents { get; set; }
        StateCollection States { get; set; }
        public void BuildCategories() {
            _MainCategory = new Category() { Name = "All", IsIncludeAll = true };
            _Films = new Category() { Name = "Films" };
            _Games = new Category() { Name = "Games" };
            _MainCategory.Categories.Add(_Films);
            _MainCategory.Categories.Add(_Games);
        }
        public void BuildStates() {
            _StateAll = new State() { Name = "All", StateId = StateId.All, IsIncludeAll = true, IconUri = new Uri("Images/All.png", UriKind.Relative) };
            _Ready = new State() { Name = "Ready", StateId = StateId.Ready, IconUri = new Uri("Images/Completed.png", UriKind.Relative) };
            _InProgress = new State() { Name = "In Progress", StateId = StateId.InProgress, IconUri = new Uri("Images/Downloading.png", UriKind.Relative) };
            _Stopped = new State() { Name = "Stopped", StateId = StateId.Stopped, IconUri = new Uri("Images/Stopped.png", UriKind.Relative) };
            _Distributed = new State() { Name = "Distributed", StateId = StateId.Distributed, IconUri = new Uri("Images/Distributed.png", UriKind.Relative) };
            _Paused = new State() { Name = "Paused", StateId = StateId.Paused, IconUri = new Uri("Images/Pause.png", UriKind.Relative) };
            States = new StateCollection() { _StateAll, _Ready, _InProgress, _Stopped, _Distributed, _Paused };
        }
        public void BuildTorrents() {
            Torrent skyrimTorrent = new Torrent(States) { Name = "Skyrim", Path = @"D:\Torrents", Size = 500.4, State = _Stopped, Number = 1, Category = _Games };
            skyrimTorrent.Files.Add(new TorrentFile(skyrimTorrent) { Size = 500.4, Path = @"D:\Torrents\Skyrim.iso", Priority = Priority.High });
            skyrimTorrent.Files.Add(new TorrentFile(skyrimTorrent) { Size = 100, Path = @"D:\Torrents\Addon.add", Priority = Priority.Low });
            Torrent kPaxTorrent = new Torrent(States) { Name = "K-Pax", Path = @"D:\Torrents", Size = 700, DownloadedSize = 700, State = _Ready, Category = _Films };
            kPaxTorrent.Files.Add(new TorrentFile(kPaxTorrent) { Path = @"D:\Torrents\K-Pax.avi", Size = 700, DownloadedSize = 700 });
            Torrent pausedTorrent = CreateTorrent();
            pausedTorrent.State = States[StateId.Paused];
            Torrent runnedTorrent = CreateTorrent();
            runnedTorrent.StartDownload();
            Torrent distributedTorrent = CreateTorrent();
            foreach(var file in distributedTorrent.Files) {
                file.DownloadedSize = file.Size;
            }
            distributedTorrent.DownloadedSize = distributedTorrent.Size;
            distributedTorrent.State = States[StateId.Distributed];
            distributedTorrent.StartDownload();
            Torrents = new ObservableCollection<Torrent>() { 
                skyrimTorrent, kPaxTorrent, pausedTorrent, runnedTorrent, distributedTorrent
            };
        }
        public Torrent CreateTorrent() {
            string name = SampleNames[_Random.Next(SampleNames.Count)];
            string path = SamplePaths[_Random.Next(SamplePaths.Count)];
            Torrent newTorrent = new Torrent(States) { State = States[StateId.Stopped], Path = path, Name = name };
            for(int i = 0; i < _Random.Next(2) + 1; i++) {
                newTorrent.Files.Add(CreateTorrentFile(newTorrent));
            }
            return newTorrent;
        }
        TorrentFile CreateTorrentFile(Torrent owner) {
            string ext = SampleExtensions[_Random.Next(SampleExtensions.Count)];
            string path = string.Format("{0}{1}.{2}", owner.Path, owner.Name, ext);
            double size = _Random.NextDouble() * _Random.Next(1000);
            return new TorrentFile(owner) { Path = path, Size = size };
        }
        public StateCollection GetStates() {
            return States;
        }
        public ObservableCollection<Category> GetCategories() {
            return new ObservableCollection<Category>() { 
                _MainCategory
            };
        }
        public ObservableCollection<Torrent> GetTorrents() {
            return Torrents;
        }
    }
}
