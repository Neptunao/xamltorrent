﻿using System.Windows;
using System.Windows.Input;

namespace xamlTorrent {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }
        private void CanExecuteClose(object sender, CanExecuteRoutedEventArgs e) {
            e.CanExecute = true;
        }
        private void ExecuteClose(object sender, ExecutedRoutedEventArgs e) {
            Close();
        }
    }
}
