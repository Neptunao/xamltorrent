﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace xamlTorrent.ViewModel {
    public class CreateNewViewModel : DependencyObject {
        public static readonly DependencyProperty ShouldRunProperty = DependencyProperty.Register("ShouldRun", typeof(bool), typeof(CreateNewViewModel),
            new PropertyMetadata(true));
        public static readonly DependencyProperty SourceFilenameProperty = DependencyProperty.Register("SourceFilename", typeof(string), typeof(CreateNewViewModel),
            new PropertyMetadata((d, e) => ((CreateNewViewModel)d).UpdateIsValid()));
        public static readonly DependencyProperty ChooseSourceCommandProperty = DependencyProperty.Register("ChooseSource", typeof(DelegateCommand), typeof(CreateNewViewModel));
        public static readonly DependencyProperty TrackersProperty = DependencyProperty.Register("Trackers", typeof(string), typeof(CreateNewViewModel),
            new PropertyMetadata(@"http://rutracker.org/", (d, e) => ((CreateNewViewModel)d).UpdateIsValid()));
        static readonly DependencyPropertyKey IsValidPropertyKey = DependencyProperty.RegisterReadOnly("IsValid", typeof(bool), typeof(CreateNewViewModel), new PropertyMetadata());
        public static readonly DependencyProperty IsValidProperty = IsValidPropertyKey.DependencyProperty;

        public CreateNewViewModel() {
            ChooseSourceCommand = new DelegateCommand(o => ChooseSource());
        }
        private void ChooseSource() {
            OpenFileDialog dlg = new OpenFileDialog();
            if(!(dlg.ShowDialog() == true)) return;
            SourceFilename = dlg.FileName;
        }
        private void UpdateIsValid() {
            IsValid = !string.IsNullOrEmpty(SourceFilename) && !string.IsNullOrEmpty(Trackers);
        }
        public DelegateCommand ChooseSourceCommand {
            get { return (DelegateCommand)GetValue(ChooseSourceCommandProperty); }
            set { SetValue(ChooseSourceCommandProperty, value); }
        }
        public string SourceFilename {
            get { return (string)GetValue(SourceFilenameProperty); }
            set { SetValue(SourceFilenameProperty, value); }
        }
        public bool ShouldRun {
            get { return (bool)GetValue(ShouldRunProperty); }
            set { SetValue(ShouldRunProperty, value); }
        }
        public bool IsValid {
            get { return (bool)GetValue(IsValidProperty); }
            private set { SetValue(IsValidPropertyKey, value); }
        }
        public string Trackers {
            get { return (string)GetValue(TrackersProperty); }
            set { SetValue(TrackersProperty, value); }
        }
    }
}
