﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace xamlTorrent.ViewModel {
    public class ContentControlExtensions : DependencyObject {
        public static readonly DependencyProperty ImageUriProperty =
            DependencyProperty.RegisterAttached("ImageUri", typeof(Uri), typeof(ContentControlExtensions), new PropertyMetadata(OnImageUriChanged));

        public static Uri GetImageUri(DependencyObject obj) {
            return (Uri)obj.GetValue(ImageUriProperty);
        }
        public static void SetImageUri(DependencyObject obj, Uri value) {
            obj.SetValue(ImageUriProperty, value);
        }
        private static void OnImageUriChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            ContentControl control = d as ContentControl;
            if(e.NewValue == null || control == null) return;
            BitmapImage bitmap = new BitmapImage((Uri)e.NewValue);
            control.Content = new Image() { Source = bitmap };
        }
    }
}
