﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace xamlTorrent.ViewModel {
    public class AddFromURLWindowViewModel : DependencyObject {
        public static readonly DependencyProperty UrlProperty = DependencyProperty.Register("Url", typeof(string), typeof(AddFromURLWindowViewModel), 
            new PropertyMetadata(@"http://torrents.com/test.torrent"));
        public string Url {
            get { return (string)GetValue(UrlProperty); }
            set { SetValue(UrlProperty, value); }
        }
    }
}
