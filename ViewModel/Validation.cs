﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace xamlTorrent.ViewModel {
    public class UrlValidationRule : ValidationRule {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo) {
            ValidationResult validationFailed = new ValidationResult(false, "URL is not well formed");
            return Uri.IsWellFormedUriString(value.ToString(), UriKind.Absolute) ? new ValidationResult(true, null) : validationFailed;
        }
    }
}
