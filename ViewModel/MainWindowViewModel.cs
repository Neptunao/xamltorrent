﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using xamlTorrent.Model;

namespace xamlTorrent.ViewModel {
    public class MainWindowViewModel : DependencyObject {
        private SeedDataBuilder builder;
        static readonly DependencyPropertyKey CategoriesPropertyKey;
        public static readonly DependencyProperty CategoriesProperty;
        static readonly DependencyPropertyKey StatesPropertyKey;
        public static readonly DependencyProperty StatesProperty;
        public static readonly DependencyProperty TorrentsProperty;
        public static readonly DependencyProperty CurrentCategoryProperty;
        public static readonly DependencyProperty CurrentStateProperty;
        public static readonly DependencyProperty CurrentTorrentsProperty;
        public static readonly DependencyProperty SelectedTorrentProperty;
        public static readonly DependencyProperty DeleteTorrentCommandProperty;
        public static readonly DependencyProperty RunCommandProperty;
        public static readonly DependencyProperty AddTorrentCommandProperty;
        public static readonly DependencyProperty AddTorrentFromURLCommandProperty;
        public static readonly DependencyProperty CreateNewCommandProperty;

        static MainWindowViewModel() {
            Type ownerType = typeof(MainWindowViewModel);
            CategoriesPropertyKey = DependencyProperty.RegisterReadOnly("Categories", typeof(ObservableCollection<Category>), ownerType,
                new PropertyMetadata(new ObservableCollection<Category>()));
            CategoriesProperty = CategoriesPropertyKey.DependencyProperty;
            StatesPropertyKey = DependencyProperty.RegisterReadOnly("States", typeof(StateCollection), ownerType, new PropertyMetadata());
            StatesProperty = StatesPropertyKey.DependencyProperty;
            TorrentsProperty = DependencyProperty.Register("Torrents", typeof(ObservableCollection<Torrent>), typeof(MainWindowViewModel),
                new PropertyMetadata(new ObservableCollection<Torrent>()));
            CurrentCategoryProperty = DependencyProperty.Register("CurrentCategory", typeof(Category), typeof(MainWindowViewModel),
                new PropertyMetadata((d, e) => ((MainWindowViewModel)d).UpdateCurrentTorrents()));
            CurrentStateProperty = DependencyProperty.Register("CurrentState", typeof(State), typeof(MainWindowViewModel),
                new PropertyMetadata((d, e) => ((MainWindowViewModel)d).UpdateCurrentTorrents()));
            CurrentTorrentsProperty = DependencyProperty.Register("CurrentTorrents", typeof(ObservableCollection<Torrent>), typeof(MainWindowViewModel));
            SelectedTorrentProperty = DependencyProperty.Register("SelectedTorrent", typeof(Torrent), typeof(MainWindowViewModel),
                new PropertyMetadata((d, e) => ((MainWindowViewModel)d).OnSelectedTorrentChanged()));
            DeleteTorrentCommandProperty = DependencyProperty.Register("DeleteTorrentCommand", typeof(DelegateCommand), typeof(MainWindowViewModel));
            RunCommandProperty = DependencyProperty.Register("RunCommand", typeof(DelegateCommand), typeof(MainWindowViewModel));
            AddTorrentCommandProperty = DependencyProperty.Register("AddTorrentCommand", typeof(DelegateCommand), typeof(MainWindowViewModel));
            AddTorrentFromURLCommandProperty = DependencyProperty.Register("AddTorrentFromURLCommand", typeof(DelegateCommand), typeof(MainWindowViewModel));
            CreateNewCommandProperty = DependencyProperty.Register("CreateNewCommand", typeof(DelegateCommand), typeof(MainWindowViewModel));
        }
        public MainWindowViewModel() {
            SeedData();
            Torrents.CollectionChanged += (s, e) => UpdateCurrentTorrents();
            DeleteTorrentCommand = new DelegateCommand(o => DeleteTorrent(), o => SelectedTorrent != null);
            RunCommand = new DelegateCommand(o => RunTorrent(), o => SelectedTorrent != null);
            AddTorrentCommand = new DelegateCommand(o => AddTorrentFromFile());
            AddTorrentFromURLCommand = new DelegateCommand(o => AddTorrentFromURL());
            CreateNewCommand = new DelegateCommand(o => CreateNewTorrent());
            CurrentTorrents = Torrents;
        }
        private void SeedData() {
            this.builder = new SeedDataBuilder();
            this.builder.BuildCategories();
            this.builder.BuildStates();
            this.builder.BuildTorrents();

            Categories = this.builder.GetCategories();
            States = this.builder.GetStates();
            Torrents = this.builder.GetTorrents();
        }
        private void RunTorrent() {
            switch(SelectedTorrent.State.StateId) {
                case StateId.Ready:
                    SelectedTorrent.State = States[StateId.Distributed];
                    break;
                case StateId.InProgress:
                case StateId.Stopped:
                    SelectedTorrent.State = States[StateId.InProgress];
                    break;
            }
            SelectedTorrent.StartDownload();
        }
        private void DeleteTorrent() {
            Torrents.Remove(SelectedTorrent);
        }
        private void AddTorrentFromFile() {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Torrents (.torrent)|*.torrent";
            if(!(dlg.ShowDialog() == true)) return;
            Torrents.Add(this.builder.CreateTorrent());
        }
        private void AddTorrentFromURL() {
            Window dialog = new AddFromURLWindow() { DataContext = new AddFromURLWindowViewModel() };
            if(!(dialog.ShowDialog() == true)) return;
            Torrents.Add(this.builder.CreateTorrent());
        }
        private void CreateNewTorrent() {
            CreateNewViewModel vm = new CreateNewViewModel();
            Window dialog = new CreateNewWindow() { DataContext = vm };
            if(!(dialog.ShowDialog() == true)) return;
            Torrent torrent = this.builder.CreateTorrent();
            torrent.DownloadedSize = torrent.Size;
            torrent.State = vm.ShouldRun ? States[StateId.Distributed] : States[StateId.Stopped];
            foreach(var file in torrent.Files) {
                file.DownloadedSize = file.Size;
            }
            Torrents.Add(torrent);
        }
        private void OnSelectedTorrentChanged() {
            DeleteTorrentCommand.RaiseCanExecuteChanged();
            RunCommand.RaiseCanExecuteChanged();
        }
        private void UpdateCurrentTorrents() {
            IEnumerable<Torrent> torrentsFromCategory = CurrentCategory != null ? Torrents.Where(t => CurrentCategory.Include(t)) : Torrents;
            IEnumerable<Torrent> torrentsFromState = CurrentState != null ? Torrents.Where(t => CurrentState.Include(t)) : Torrents;
            CurrentTorrents = new ObservableCollection<Torrent>(torrentsFromCategory.Intersect(torrentsFromState));
        }
        public ObservableCollection<Category> Categories {
            get { return (ObservableCollection<Category>)GetValue(CategoriesProperty); }
            private set { SetValue(CategoriesPropertyKey, value); }
        }
        public StateCollection States {
            get { return (StateCollection)GetValue(StatesProperty); }
            private set { SetValue(StatesPropertyKey, value); }
        }
        public ObservableCollection<Torrent> Torrents {
            get { return (ObservableCollection<Torrent>)GetValue(TorrentsProperty); }
            set { SetValue(TorrentsProperty, value); }
        }
        public State CurrentState {
            get { return (State)GetValue(CurrentStateProperty); }
            set { SetValue(CurrentStateProperty, value); }
        }
        public Category CurrentCategory {
            get { return (Category)GetValue(CurrentCategoryProperty); }
            set { SetValue(CurrentCategoryProperty, value); }
        }
        public ObservableCollection<Torrent> CurrentTorrents {
            get { return (ObservableCollection<Torrent>)GetValue(CurrentTorrentsProperty); }
            set { SetValue(CurrentTorrentsProperty, value); }
        }
        public Torrent SelectedTorrent {
            get { return (Torrent)GetValue(SelectedTorrentProperty); }
            set { SetValue(SelectedTorrentProperty, value); }
        }
        public DelegateCommand DeleteTorrentCommand {
            get { return (DelegateCommand)GetValue(DeleteTorrentCommandProperty); }
            set { SetValue(DeleteTorrentCommandProperty, value); }
        }
        public DelegateCommand RunCommand {
            get { return (DelegateCommand)GetValue(RunCommandProperty); }
            set { SetValue(RunCommandProperty, value); }
        }
        public DelegateCommand AddTorrentCommand {
            get { return (DelegateCommand)GetValue(AddTorrentCommandProperty); }
            set { SetValue(AddTorrentCommandProperty, value); }
        }
        public DelegateCommand AddTorrentFromURLCommand {
            get { return (DelegateCommand)GetValue(AddTorrentFromURLCommandProperty); }
            set { SetValue(AddTorrentFromURLCommandProperty, value); }
        }
        public DelegateCommand CreateNewCommand {
            get { return (DelegateCommand)GetValue(CreateNewCommandProperty); }
            set { SetValue(CreateNewCommandProperty, value); }
        }
    }
}
