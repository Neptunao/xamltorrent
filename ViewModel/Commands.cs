﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace xamlTorrent.ViewModel {
    public class DelegateCommand<T> : ICommand {
        private readonly Predicate<T> canExecuteHandler;
        private readonly Action<T> executeHandler;
        public DelegateCommand(Action<T> executeHandler) {
            this.executeHandler = executeHandler;
            this.canExecuteHandler = (obj => true);
        }
        public DelegateCommand(Action<T> executeHandler, Predicate<T> canExecuteHandler) {
            this.executeHandler = executeHandler;
            this.canExecuteHandler = canExecuteHandler;
        }
        public bool CanExecute(object parameter) {
            return this.canExecuteHandler((T)parameter);
        }
        public event EventHandler CanExecuteChanged;
        public void Execute(object parameter) {
            this.executeHandler((T)parameter);
        }
        public void RaiseCanExecuteChanged() {
            if(CanExecuteChanged == null) return;
            CanExecuteChanged(this, new EventArgs());
        }
    }
    public class DelegateCommand : DelegateCommand<object> {
        public DelegateCommand(Action<object> executeHandler) : base(executeHandler) { }
        public DelegateCommand(Action<object> executeHandler, Predicate<object> canExecuteHandler) : base(executeHandler, canExecuteHandler) { }
    }
}
