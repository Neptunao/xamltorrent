﻿<Window x:Class="xamlTorrent.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:model="clr-namespace:xamlTorrent.Model"
        xmlns:vm="clr-namespace:xamlTorrent.ViewModel"
        xmlns:custom="clr-namespace:xamlTorrent.Controls"
        xmlns:diag="clr-namespace:System.Diagnostics;assembly=WindowsBase"
        Icon="Images/Icon.png"
        Title="xamlTorrent" Height="768" Width="1024">
    <Window.DataContext>
        <vm:MainWindowViewModel />
    </Window.DataContext>
    <Window.Resources>
        <ResourceDictionary>
            <ResourceDictionary.MergedDictionaries>
                <ResourceDictionary Source="MainPageControls.xaml" />
            </ResourceDictionary.MergedDictionaries>
            <vm:IntToPositionConverter x:Key="IntToPositionConverter" />
        </ResourceDictionary>
    </Window.Resources>
    <Window.CommandBindings>
        <CommandBinding Command="ApplicationCommands.Close" CanExecute="CanExecuteClose" Executed="ExecuteClose" />
    </Window.CommandBindings>
    <Window.InputBindings>
        <KeyBinding Gesture="CTRL+O" Command="{Binding AddTorrentCommand}" />
    </Window.InputBindings>
    <DockPanel>
        <Menu DockPanel.Dock="Top" IsMainMenu="True">
            <MenuItem Header="File">
                <MenuItem Header="Add Torrent" Command="{Binding AddTorrentFromURLCommand}" />
                <MenuItem Header="Add Torrent from URL" Command="{Binding AddTorrentCommand}" />
                <Separator />
                <MenuItem Header="Create Torrent" Command="{Binding CreateNewCommand}" />
                <Separator />
                <MenuItem Header="_Exit" Command="ApplicationCommands.Close" />
            </MenuItem>
        </Menu>
        <ToolBarTray DockPanel.Dock="Top" HorizontalAlignment="Stretch" Background="{DynamicResource NormalBrush}" >
            <ToolBar MaxHeight="45" Band="1" BandIndex="1">
                <Button Style="{StaticResource toolbarButtonStyle}" ToolTip="Add Torrent" vm:ContentControlExtensions.ImageUri="Images/AddTorrent.png" Command="{Binding AddTorrentCommand}" />
                <Button Style="{StaticResource toolbarButtonStyle}" ToolTip="Add Torrent from URL" vm:ContentControlExtensions.ImageUri="Images/AddTorrentFromURL.png" 
                        Command="{Binding AddTorrentFromURLCommand}" />
                <Separator />
                <Button Style="{StaticResource toolbarButtonStyle}" ToolTip="Create new Torrent" vm:ContentControlExtensions.ImageUri="Images/CreateNew.png" Command="{Binding CreateNewCommand}" />
                <Button Style="{StaticResource manageButtonStyle}" ToolTip="Delete Torrent" vm:ContentControlExtensions.ImageUri="Images/Remove.png" 
                        Command="{Binding DeleteTorrentCommand}" />
                <Separator />
                <Button Style="{StaticResource manageButtonStyle}" ToolTip="Start downloading" vm:ContentControlExtensions.ImageUri="Images/Start.png" Command="{Binding RunCommand}" />
                <Button Style="{StaticResource manageButtonStyle}" ToolTip="Stop downloading" vm:ContentControlExtensions.ImageUri="Images/Stop.png" Command="{Binding SelectedTorrent.StopCommand}" />
                <Button Style="{StaticResource manageButtonStyle}" ToolTip="Pause downloading" vm:ContentControlExtensions.ImageUri="Images/Pause.png" Command="{Binding SelectedTorrent.PauseCommand}" />
            </ToolBar>
        </ToolBarTray>
        <Grid>
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="Auto" MinWidth="100" />
                <ColumnDefinition Width="*" />
            </Grid.ColumnDefinitions>
            <Grid DockPanel.Dock="Left">
                <Grid.RowDefinitions>
                    <RowDefinition Height="*" />
                    <RowDefinition Height="*" />
                </Grid.RowDefinitions>
                <ListBox ItemsSource="{Binding States}" ItemTemplate="{StaticResource stateTemplate}" SelectedItem="{Binding CurrentState, Mode=TwoWay}" />
                <custom:BindableTreeView x:Name="categoryTree" Grid.Row="1" ItemsSource="{Binding Categories}" 
                                         ItemTemplate="{StaticResource categoryTemplate}" SelectedNode="{Binding CurrentCategory, Mode=OneWayToSource}" />
                <GridSplitter VerticalAlignment="Bottom" HorizontalAlignment="Stretch" Height="5" />
            </Grid>
            <Grid Grid.Column="1">
                <Grid.RowDefinitions>
                    <RowDefinition Height="*" />
                    <RowDefinition Height="Auto" />
                </Grid.RowDefinitions>
                <DataGrid ItemsSource="{Binding CurrentTorrents}" AutoGenerateColumns="False" CanUserAddRows="False" CanUserDeleteRows="False" 
                          SelectionMode="Single" SelectedItem="{Binding SelectedTorrent, Mode=TwoWay}">
                    <DataGrid.Columns>
                        <DataGridTemplateColumn Width="Auto" CellTemplate="{StaticResource stateCellTemplate}" HeaderTemplate="{StaticResource stateHeaderTemplate}" />
                        <DataGridTextColumn Width="Auto" Header="№" Binding="{Binding Number, Converter={StaticResource IntToPositionConverter}}" />
                        <DataGridTextColumn Width="*" Header="Name" Binding="{Binding Name}" />
                        <DataGridTextColumn Width="*" Header="Size" Binding="{Binding Size, StringFormat='{}{0:F1} MB'}" IsReadOnly="True" />
                        <DataGridTemplateColumn Width="*" Header="Progress" CellTemplate="{StaticResource progressTemplate}" />
                        <DataGridTemplateColumn Width="*" Header="Seeds/Peers">
                            <DataGridTemplateColumn.CellTemplate>
                                <DataTemplate>
                                    <StackPanel Orientation="Horizontal">
                                        <TextBlock Text="{Binding SeedCount, StringFormat='{}{0}/'}" />
                                        <TextBlock Text="{Binding PeerCount}" />
                                    </StackPanel>
                                </DataTemplate>
                            </DataGridTemplateColumn.CellTemplate>
                        </DataGridTemplateColumn>
                    </DataGrid.Columns>
                </DataGrid>
                <Border BorderThickness="0.2,0,0,0" BorderBrush="Blue" Background="{DynamicResource TabControlHeaderBrush}" Grid.Row="1" >
                    <TabControl x:Name="infoTabs" Style="{StaticResource infoTabStyle}" MinHeight="150">
                        <TabItem Header="Information" HorizontalContentAlignment="Stretch" VerticalContentAlignment="Stretch">
                            <StackPanel>
                                <StackPanel Orientation="Horizontal">
                                    <TextBlock Text="Size: " />
                                    <TextBlock Text="{Binding SelectedTorrent.Size, StringFormat='{}{0:F1} MB'}" />
                                </StackPanel>
                                <StackPanel Orientation="Horizontal">
                                    <TextBlock Text="Downloaded: " />
                                    <TextBlock Text="{Binding SelectedTorrent.DownloadedSize, StringFormat='{}{0:F1} MB', Delay=100}" />
                                </StackPanel>
                                <StackPanel Orientation="Horizontal">
                                    <TextBlock Text="Seeds: " />
                                    <TextBlock Text="{Binding SelectedTorrent.SeedCount}" />
                                </StackPanel>
                                <StackPanel Orientation="Horizontal">
                                    <TextBlock Text="Peers: " />
                                    <TextBlock Text="{Binding SelectedTorrent.PeerCount}" />
                                </StackPanel>
                                <StackPanel Orientation="Horizontal">
                                    <StackPanel.Resources>
                                        <Style TargetType="TextBlock" BasedOn="{StaticResource activeTorrentInfoStyle}" />
                                    </StackPanel.Resources>
                                    <TextBlock Style="{StaticResource speedLableStyle}" />
                                    <TextBlock Text="{Binding SelectedTorrent.Speed, StringFormat='{}{0:F1} KB/s'}" />
                                </StackPanel>
                                <StackPanel Orientation="Horizontal">
                                    <TextBlock Text="Started At: " />
                                    <TextBlock Text="{Binding SelectedTorrent.StartedAt}" />
                                </StackPanel>
                            </StackPanel>
                        </TabItem>
                        <TabItem Header="Files" HorizontalContentAlignment="Stretch" VerticalContentAlignment="Stretch">
                            <DataGrid AutoGenerateColumns="False" CanUserAddRows="False" CanUserDeleteRows="False" ItemsSource="{Binding SelectedTorrent.Files}">
                                <DataGrid.Columns>
                                    <DataGridTextColumn Width="*" Header="Path" Binding="{Binding Path}" IsReadOnly="True"  />
                                    <DataGridTextColumn Width="*" Header="Size" Binding="{Binding Size, StringFormat='{}{0:F1} MB'}" IsReadOnly="True" />
                                    <DataGridTemplateColumn Width="*" Header="Progress" CellTemplate="{StaticResource progressTemplate}" IsReadOnly="True" />
                                    <DataGridComboBoxColumn Width="*" Header="Priority" ItemsSource="{x:Static model:TorrentFile.Priorities}" 
                                                        SelectedItemBinding="{Binding Priority, Mode=TwoWay}" IsReadOnly="False" />
                                </DataGrid.Columns>
                            </DataGrid>
                        </TabItem>
                    </TabControl>
                </Border>
                <GridSplitter Visibility="{Binding Visibility, ElementName=infoTabs}" VerticalAlignment="Bottom" HorizontalAlignment="Stretch" Height="5" />
            </Grid>
            <GridSplitter Grid.Column="0" Width="5" HorizontalAlignment="Right" VerticalAlignment="Stretch"/>
        </Grid>
    </DockPanel>
</Window>
